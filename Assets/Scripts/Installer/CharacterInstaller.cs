using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Zenject;

public class CharacterInstaller : MonoInstaller
{
	[SerializeField] private MoveController _moveController;
	[SerializeField] private CameraHandler _cameraHandler;
	[SerializeField] private AnimatorController _animatorController;
	[SerializeField] private InputController _inputControllerl;
	[SerializeField] private MeleeAttackController _meleeAttack;
	[SerializeField] private LockOnTargetController _lockOnTargetController;
	[SerializeField] private RotateController _rotateController;

	private List<IController> controllers;

    public override void InstallBindings()
    {
		Container.Bind<MoveController>().FromInstance(_moveController).AsSingle();
		Container.Bind<CameraHandler>().FromInstance(_cameraHandler).AsSingle();
		Container.Bind<AnimatorController>().FromInstance(_animatorController).AsSingle();
		Container.Bind<InputController>().FromInstance(_inputControllerl).AsSingle();

		Container.Bind<MeleeAttackController>().FromInstance(_meleeAttack).AsSingle();
		Container.Bind<LockOnTargetController>().FromInstance(_lockOnTargetController).AsSingle();
		Container.Bind<RotateController>().FromInstance(_rotateController).AsSingle();

		controllers = new List<IController>();
		controllers.Add(_moveController);
		controllers.Add(_animatorController);
		controllers.Add(_inputControllerl);
		controllers.Add(_meleeAttack);
		controllers.Add(_lockOnTargetController);
		controllers.Add(_rotateController);
	}

	private void Update()
	{
		foreach(var controller in controllers)
		{
			controller.ControllerUpdate();
		}
	}

	private void LateUpdate()
	{
		_cameraHandler.CameraUpdate();
	}
}