﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MoveController : MonoBehaviour, IController
{
	public float CurrentSpeed { get; private set; }

	[SerializeField] private float _standartSpeed;

	[SerializeField] private float _sprintSpeed;

	[SerializeField] private GameObject character;

	[SerializeField] private Transform _characterBack;

	[Inject] private InputController _inputController;

	[Inject] private MeleeAttackController _meleeAttack;

	public Vector3 MoveDirection { get; private set; }

	private CharacterController _characterController;

	public float HorizontalInput { get; private set; }

	public float VerticalInput { get; private set; }

	private void Start()
	{
		_characterController = character.GetComponent<CharacterController>();
		_inputController.SprintButtonHolding += SetSprintSpeed;
		_inputController.SprintButtonEndHolding += SetStandartSpeed;
		SetStandartSpeed();
	}

	private void OnDestroy()
	{
		_inputController.SprintButtonHolding -= SetSprintSpeed;
		_inputController.SprintButtonEndHolding -= SetStandartSpeed;
	}

	public void ControllerUpdate()
	{
		HorizontalInput = Input.GetAxis("Horizontal");
		VerticalInput = Input.GetAxis("Vertical");

		FreeMove();
	}

	private void FreeMove()
	{
		if (!_meleeAttack.IsFirstAttack && !_meleeAttack.IsSecondAttack)
		{
			MoveDirection = new Vector3(HorizontalInput, 0, VerticalInput);

			MoveDirection = Camera.main.transform.TransformDirection(MoveDirection);

			MoveDirection = new Vector3(MoveDirection.x, 0, MoveDirection.z);

			_characterController.Move(MoveDirection * Time.deltaTime * CurrentSpeed);
		}
	}

	private void SetSprintSpeed()
	{
		CurrentSpeed = _sprintSpeed;
	}

	private void SetStandartSpeed()
	{
		CurrentSpeed = _standartSpeed;
	}
}
