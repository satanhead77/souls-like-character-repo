﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public class AnimatorController : MonoBehaviour, IController
{
	public event Action BlockAnimationEnbaled;
	public event Action BlockAnimationDisabled;
	[SerializeField] private GameObject _character;
	[SerializeField] private Animator _animator;
	[Inject] private MoveController _moveController;
	[Inject] private InputController _inputController;
	[Inject] private MeleeAttackController _meleeAttack;
	//[Inject] private LockOnTargetController _lockOnTargetController;
	[field:SerializeField] public bool IsRolling { get; private set; }
	[SerializeField] private bool _isQuickAttack;
	[SerializeField] private bool _isBlocking;


	private void Start()
	{
		_animator = _character.GetComponent<Animator>();
		_inputController.BlockButtonHolding += EnableBlock;
		_inputController.BlockButtonEndHolding += DisableBlock;

		_inputController.RollButtonPressed += EnableRoll;
		 
	}

	private void OnDestroy()
	{
		_inputController.BlockButtonHolding -= EnableBlock;
		_inputController.BlockButtonEndHolding -= DisableBlock;

		_inputController.RollButtonPressed -= EnableRoll;
	}

	public void ControllerUpdate()
	{
		AnimateCharacter();
	}

	private void AnimateCharacter()
	{
		if (IsRolling)
		{
			_animator.SetBool("Roll", true);
		}
		else
		{
			_animator.SetBool("Roll", false);

			_animator.SetFloat("Blend", _moveController.CurrentSpeed);

			_animator.SetFloat("forwardMove", _moveController.MoveDirection.z);
			_animator.SetFloat("sideMove", _moveController.MoveDirection.x);
		}

		AnimatorSetBool("Quick Attack", _meleeAttack.IsFirstAttack);

		AnimatorSetBool("Second Attack", _meleeAttack.IsSecondAttack);

		//AnimatorSetBool("Battle Run Mode", _lockOnTargetController.TargetInLock);
		 
		if (_isBlocking)
		{
			_animator.SetBool("Block", true);
			BlockAnimationEnbaled?.Invoke();
		}
		else
		{
			_animator.SetBool("Block", false);
			BlockAnimationDisabled?.Invoke();
		}
	}

	private void AnimatorSetBool(string animControllerValue, bool value)
	{
		if (value)
		{
			_animator.SetBool(animControllerValue, true);
		}
		else
		{
			_animator.SetBool(animControllerValue, false);
		}
	}

	private void EnableBlock()
	{
		_isBlocking = true;
	}

	private void DisableBlock()
	{
		_isBlocking = false;
	}

	private void EnableRoll()
	{
		StartCoroutine(RollingEnumerator());
	}

	private IEnumerator RollingEnumerator()
	{
		IsRolling = true; 
		yield return new WaitForSeconds(1f);
		IsRolling = false;
	}

}
