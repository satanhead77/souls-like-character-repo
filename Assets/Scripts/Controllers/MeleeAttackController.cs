﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MeleeAttackController : MonoBehaviour, IController
{
	[SerializeField] private float _comboTime;
	private float _startComboTime;
	[field: SerializeField] public bool IsFirstAttack { get; private set; }
	[field: SerializeField] public bool IsSecondAttack { get; private set; }
	[SerializeField] private bool _comboIsActive;
	[SerializeField] private bool _comboTimerIsEnabled;

	[Inject] private InputController _inputController;

	private void Start()
	{
		_startComboTime = _comboTime;

		_inputController.QuickAttackButtonPressed += Attack;
	}

	private void OnDestroy()
	{
		_inputController.QuickAttackButtonPressed -= Attack;
	}

	public void ControllerUpdate()
	{
		ComboEnable();
		ComboTimer();
	}

	public void Attack()
	{
		if (!_comboTimerIsEnabled && !IsFirstAttack)
		{
			StartCoroutine(FirstAttackEnumerator());
		}
		if (_comboTimerIsEnabled && !IsFirstAttack && !IsSecondAttack)
		{
			StartCoroutine(SecondAttackEnumerator());
		}
	}

	private void ComboEnable()
	{
		if (IsFirstAttack)
		{
			_comboTimerIsEnabled = true;
		}
	}

	private void ComboTimer()
	{
		if (_comboTimerIsEnabled)
		{
			_comboTime -= Time.deltaTime;

			if (_comboTime <= 0)
			{
				_comboTime = _startComboTime;
				_comboTimerIsEnabled = false;
				_comboIsActive = false;
			}
			else
			{
				_comboIsActive = true;
			}
		}
	}

	private IEnumerator FirstAttackEnumerator()
	{
		IsFirstAttack = true;
		yield return new WaitForSeconds(0.5f);
		IsFirstAttack = false;
	}

	private IEnumerator SecondAttackEnumerator()
	{
		IsSecondAttack = true;
		yield return new WaitForSeconds(0.8f);
		IsSecondAttack = false;
		_comboTime = 0;
	}

 
}
