﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class IkGoal : MonoBehaviour
{
	private Animator _characterAnimator;
	[SerializeField] private Transform _standartBodyPos;
	[SerializeField] private Transform _bodyTargetInBlock;
	[SerializeField] private bool _isblock;
	[Inject] private AnimatorController _animatorController;

	private void Start()
	{
		_characterAnimator = GetComponent<Animator>();

		_animatorController.BlockAnimationEnbaled += EnableBlockValue;
		_animatorController.BlockAnimationDisabled += DisableBlock;
	}

	private void OnDestroy()
	{
		_animatorController.BlockAnimationEnbaled -= EnableBlockValue;
		_animatorController.BlockAnimationDisabled -= DisableBlock;
	}

	private void OnAnimatorIK(int layerIndex)
	{
		if (!_animatorController.IsRolling)
		{
			_characterAnimator.SetLookAtWeight(1, 1);
			SetBodyAndLeftHandPositionInBlock();
		}
		else
		{
			_characterAnimator.SetLookAtWeight(0, 0);
		}
	}

	private void SetBodyAndLeftHandPositionInBlock()
	{
		if (_isblock)
		{
			_characterAnimator.SetLookAtPosition(_bodyTargetInBlock.position);
		}
		else
		{
			_characterAnimator.SetLookAtPosition(_standartBodyPos.position);
		}
	}

	private void EnableBlockValue()
	{
		_isblock = true;
	}


	private void DisableBlock()
	{
		_isblock = false;
	}

	
}