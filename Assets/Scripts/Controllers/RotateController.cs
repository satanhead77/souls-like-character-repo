﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class RotateController : MonoBehaviour , IController
{
	[SerializeField] private float _rotateSpeed;

	[SerializeField] private GameObject character;

	[Inject] private AnimatorController _animatorController;

	[Inject] private MoveController _moveController;

	public void ControllerUpdate()
	{
		RotateToMoveDirection();
	}

	private void RotateToMoveDirection()
	{
		if (!_animatorController.IsRolling)
		{
			if (Mathf.Abs(_moveController.VerticalInput) > 0 || Mathf.Abs(_moveController.HorizontalInput) > 0)
			{
				character.transform.rotation = Quaternion.Lerp(character.transform.rotation, Quaternion.LookRotation(_moveController.MoveDirection),
				_rotateSpeed * Time.deltaTime);
			}
		}
	}
}
