﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

public class LockOnTargetController : MonoBehaviour, IController
{
	public Character currentLockTarget;
	[SerializeField] private Transform _camera;
	[SerializeField] private Transform _character;
	[SerializeField] private float _maxDistFromCharacterToTarget;
	[field: SerializeField] public bool TargetInLock { get; private set; }
	[field: SerializeField] public Vector3 CharacterLookDirection { get; private set; }
	[SerializeField] private List<Character> _targetsThatCanBeTakenInLock;
	[SerializeField] private List<Character> _allTargets;
	[SerializeField] private List<Character> _leftTargets;
	[SerializeField] private List<Character> _rightTargets;
	[Inject] private InputController _inputController;

	private void Start()
	{
		_inputController.TakeTargetInLockButtonPressed += Lock;
	}

	private void OnDestroy()
	{
		_inputController.TakeTargetInLockButtonPressed -= Lock;
	}

	public void ControllerUpdate()
	{
		RemoveTargetFromTargetsList();
		AddNewTargetToTargetsList();
		SortTargetsByPosition();

		ChangeLockTarget();

		if (TargetInLock)
		{
			AddLeftAndRightTargetsToLists();
			CharacterLookDirection = currentLockTarget.transform.position - _character.position;
		}
		LockDisable();
	}

	private void SetCeurrentLockTarget()
	{
		if (_targetsThatCanBeTakenInLock.Count > 0 && !TargetInLock)
		{
			currentLockTarget = _targetsThatCanBeTakenInLock[0];
		}
	}

	private void Lock()
	{
		if (!TargetInLock && _targetsThatCanBeTakenInLock.Count > 0)
		{
			TargetInLock = true;
		}
		else
		{
			TargetInLock = false;
		}
	}

	private void LockDisable()
	{
		if (TargetInLock)
		{
			if (_targetsThatCanBeTakenInLock.Count == 0)
			{
				TargetInLock = false;
			}
		}
	}

	private void AddNewTargetToTargetsList()
	{
		for (int i = 0; i < _allTargets.Count; ++i)
		{
			float dist = Vector3.Distance(_character.position, _allTargets[i].transform.position);

			if (dist <= _maxDistFromCharacterToTarget && FindTheTargetInFrontOfTheCamera(_allTargets[i].transform))
			{
				_allTargets[i].AddCharacterToTargetList(_targetsThatCanBeTakenInLock);
				SetCeurrentLockTarget();
			}
		} 
	}

	private void RemoveTargetFromTargetsList()
	{
		if (_targetsThatCanBeTakenInLock.Count > 0)
		{
			for (int i = 0; i < _targetsThatCanBeTakenInLock.Count; ++i)
			{
				float dist = Vector3.Distance(_character.position, _targetsThatCanBeTakenInLock[i].transform.position);

				if (dist > _maxDistFromCharacterToTarget || !FindTheTargetInFrontOfTheCamera(_targetsThatCanBeTakenInLock[i].transform))
				{
					_targetsThatCanBeTakenInLock[i].RemoveCharacterFromTargetList(_targetsThatCanBeTakenInLock);
				}
			}
		}
	}

	private void SortTargetsByPosition()
	{
		if (_targetsThatCanBeTakenInLock.Count > 1 && !TargetInLock)
		{
			_targetsThatCanBeTakenInLock.Sort((target1, target2) =>
			{
				var distance1 = (_character.position - target1.transform.position).sqrMagnitude;
				var distance2 = (_character.position - target2.transform.position).sqrMagnitude;

				if (distance1 > distance2) return 1;
				else return -1;
				});
		}
	}

	private bool FindTheTargetInFrontOfTheCamera(Transform target)
	{
		Vector3 cameraToTarget = target.position - _camera.position;

		float cameraViewAngle = Vector3.Angle(cameraToTarget, _camera.forward);

		if (cameraViewAngle > -40 && cameraViewAngle < 40)
		{
			return true;
		}

		return false;
	}

	private void AddLeftAndRightTargetsToLists()
	{
		_leftTargets.Clear();
		_rightTargets.Clear();

		foreach (var target in _targetsThatCanBeTakenInLock)
		{
			Vector3 targetPos = Camera.main.WorldToViewportPoint(target.transform.position);

			if (targetPos.x > 0.5f)
			{
				print("target is on the right side! " + target.name);

				if (target != currentLockTarget)
				{
					_rightTargets.Add(target);
				}
			}

			if (targetPos.x < 0.5f)
			{
				print("target is on the left side! " + target.name);

				if (target != currentLockTarget)
				{
					_leftTargets.Add(target);
				}
			}
		}
	}

	private void ChangeLockTarget()
	{
		if (TargetInLock)
		{
			if (Input.GetKeyDown(KeyCode.X))
			{
				currentLockTarget = _leftTargets[0];
			}

			if (Input.GetKeyDown(KeyCode.C))
			{
				currentLockTarget = _rightTargets[0];
			}
		}
	}
}
