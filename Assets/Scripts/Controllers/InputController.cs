﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputController : MonoBehaviour,IController
{
	public event Action SprintButtonHolding;
	public event Action SprintButtonEndHolding;

	public event Action BlockButtonHolding;
	public event Action BlockButtonEndHolding;


	public event Action FightModeButtonPressed;
	public event Action RollButtonPressed;
	public event Action QuickAttackButtonPressed;
	public event Action TakeTargetInLockButtonPressed;


	[SerializeField] private KeyCode _sprintButton;
	[SerializeField] private KeyCode _fightModeButton;
	[SerializeField] private KeyCode _rollButtonButton;
	[SerializeField] private KeyCode _quickAttackButton;
	[SerializeField] private KeyCode _blockButton;
	[SerializeField] private KeyCode _takeTargetInLockButton;

	public void ControllerUpdate()
	{
		ButtonPressed(_fightModeButton, FightModeButtonPressed);
		ButtonPressed(_rollButtonButton, RollButtonPressed);
		ButtonPressed(_quickAttackButton, QuickAttackButtonPressed);
		ButtonPressed(_takeTargetInLockButton, TakeTargetInLockButtonPressed);

		ButtonHolded(_sprintButton, SprintButtonHolding, SprintButtonEndHolding);
		ButtonHolded(_blockButton, BlockButtonHolding, BlockButtonEndHolding);
	}

	private void ButtonHolded(KeyCode key, Action action,Action endAction)
	{
		if (Input.GetKey(key))
		{
			action?.Invoke();
		}
		else if (Input.GetKeyUp(key))
		{
			endAction?.Invoke();
		}
	}

	private void ButtonPressed(KeyCode key , Action action)
	{
		if (Input.GetKeyDown(key))
		{
			action?.Invoke();
		}
	}
}
