﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	[field:SerializeField] public bool CharacterInTargetsList { get; private set; }

    public void AddCharacterToTargetList(List<Character> list)
	{
		if (!CharacterInTargetsList)
		{
			list.Add(this);
			CharacterInTargetsList = true;
		}
	}

	public void RemoveCharacterFromTargetList(List<Character> list)
	{
		list.Remove(this);
		CharacterInTargetsList = false;
	}
}
