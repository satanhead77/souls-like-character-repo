﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour, IController 
{
	[SerializeField] private Camera _camera;
	[SerializeField] private Transform _target;
	[SerializeField] private Transform _enemy;
	[SerializeField] private Vector3 _offset;
	[SerializeField] private float _mouseRotateSpeed;
	[SerializeField] private float _xMouseInput;
	[SerializeField] private bool _enemyInTarget;


	private void Start()
	{
		_camera.transform.position = _target.position + _offset;
	}

	private void CameraFollow()
	{
		if (!_enemyInTarget)
		{
			_camera.transform.position = _target.position + _offset;
		}
		else
		{
			float enemyZ = _enemy.position.z;
			float enemyX = _enemy.position.x;

			float characterZ = _target.position.z;
			float characterX = _target.position.x;

			float needZpos = characterZ - (enemyZ-characterZ);
			float needXpos = characterX - (enemyX - characterX);

			_camera.transform.position = new Vector3(needXpos,1, needXpos);
		}
	}

	private void CameraRotateAroundTarget()
	{
		if (!_enemyInTarget)
		{
			_xMouseInput = Input.GetAxis("Mouse X");

			Quaternion turnAngle = Quaternion.AngleAxis(_xMouseInput * _mouseRotateSpeed, Vector3.up);

			_offset = turnAngle * _offset;
		}
	}

	public void ControllerUpdate()
	{
		CameraRotateAroundTarget();
		CameraFollow();

		if (!_enemyInTarget)
		{
			_camera.transform.LookAt(_target);
		}
		else
		{
			_camera.transform.LookAt(_enemy);
		}
	}

}
