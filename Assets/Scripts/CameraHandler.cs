﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CameraHandler : MonoBehaviour
{
	[SerializeField] private Transform _target;
	[SerializeField] private Transform _camera;
	[SerializeField] private Transform _cameraPivot;
	[SerializeField] private float _lookSpeed = 0.1f;
	[SerializeField] private float _followSpeed = 0.1f;
	[SerializeField] private float _pivotSpeed = 0.3f;
	[SerializeField] private float _minPivot = -35;
	[SerializeField] private float _maxPivot = 35;

	[SerializeField] private Vector3 _pivotPosInLock;
	[SerializeField] private Vector3 _cameraPosInLock;
	[SerializeField] private float _cameraYRotationInLock = 15;

    private Vector3 _pivotStandartPos;
	private Vector3 _cameraStandartPos;
	private float _cameraYStandartRotation = 0;

	private float _lookAngle;
	private float _pivotAngle;
	private Transform _cameraHandlerTransform;


	[Inject] private LockOnTargetController _lockOnTargetController;

	private void Start()
	{
		_cameraHandlerTransform = transform;

		_pivotStandartPos = _cameraPivot.position;
		_cameraStandartPos = _camera.position;
	}

	public void CameraUpdate()
	{
		float delta = Time.deltaTime;

		FollowTarget(delta);

		HandleCameraRotation(delta, Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

		SetCameraHeight();
	}

	private void FollowTarget(float delta)
	{
		Vector3 targetPos = Vector3.Lerp(_cameraHandlerTransform.position, _target.position, delta / _followSpeed);
		_cameraHandlerTransform.position = targetPos;
	}

	private void HandleCameraRotation(float delta,float mouseXInput, float mouseYInput)
	{
		if (!_lockOnTargetController.TargetInLock)
		{
			_lookAngle += (mouseXInput * _lookSpeed) / delta;
			_pivotAngle -= (mouseYInput * _pivotSpeed) / delta;
			_pivotAngle = Mathf.Clamp(_pivotAngle, _minPivot, _maxPivot);

			Vector3 rotation = Vector3.zero;
			rotation.y = _lookAngle;
			Quaternion targetRotation = Quaternion.Euler(rotation);
			_cameraHandlerTransform.rotation = targetRotation;

			rotation = Vector3.zero;
			rotation.x = _pivotAngle;

			targetRotation = Quaternion.Euler(rotation);
			_cameraPivot.localRotation = targetRotation;
		}
		else
		{
			LockOnTarget(_lockOnTargetController.currentLockTarget);
		}
	}

	private void LockOnTarget(Character lockTarget)
	{
		Vector3 dir = lockTarget.transform.position - transform.position;
		dir.Normalize();
		dir.y = 0;

		Quaternion targetRot = Quaternion.LookRotation(dir);
		transform.rotation = targetRot;

		dir = lockTarget.transform.position - _cameraPivot.position;
		dir.Normalize();

		targetRot = Quaternion.LookRotation(dir);
		Vector3 eulerAngle = targetRot.eulerAngles;
		eulerAngle.y = 0;
		_cameraPivot.localEulerAngles = eulerAngle;
	}

	private void SetCameraHeight()
	{
		if (_lockOnTargetController.TargetInLock)
		{
			_cameraPivot.localPosition = _pivotPosInLock;
			_camera.localPosition = _cameraPosInLock;
			Vector3 eulerAngle = new Vector3(_cameraYRotationInLock, 0, 0);
			_camera.localEulerAngles = eulerAngle;
		}
		else
		{
			_cameraPivot.localPosition = _pivotStandartPos;
			_camera.localPosition = _cameraStandartPos;
			Vector3 eulerAngle = new Vector3(_cameraYStandartRotation, 0, 0);
			_camera.localEulerAngles = eulerAngle;
		}
	}
}
